---
title: About Me
banner: coffee-and-couch
---
Pastel Cravings combine my two favourite things in the world: the palette color of pastel and food. Those two together explain why I always dream about sugar and donuts. And ramen.

Trang started as an Instagrammer and post about the food until she realized she also wanted to write about it. Not in a classical fancy kind of way. She was tired of reviews restrictions and the pressure of writing to please the press. When all she ever wants is to write for her and the public. That's how she created this personal food blog where she can express her feelings about the meals without any fear.

All the sponsors or collaborations on this blog know about her transparence policy. They accept to be reviewed as it is.


**"What does she do in life a part from running a food blog?!"**

- Bachelor of Psychology, specialization in brain disorders and brain imagery. I now become a full-time research associate for the government and work on a variety of cognitive neurosciences project.

- Owns also a book blog at Bookidote.com where I can share my passion for reading. OH you can also find movie and comics reviews!

- I can eat ramen every day. A bit obssessed of going to the gym so I can et even more ramen (MUAHAH).
