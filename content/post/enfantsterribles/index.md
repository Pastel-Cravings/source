---
authors:
- Trang
banner: lesenfantsterribles4
categories:
- Restaurants
tags:
- Dinner
- Place Ville Marie
- Skyscraper
- Romantic
- Bonaventure
date: 2017-10-24T14:06:46-05:00
description: Date in Starlight
title: Les Enfants Terribles
---

**I'm going to tell you why you are going to go to this place despise of my review: the stunning view.** This place is on the 44th floor of Place Ville-Marie and has an astonishing sight of Montreal at night. It's perfect to impress your date night, that is for sure. 

However, **if you care about the quality of the food and service of your date, you might want to read further.**

Luckily for us, we were only two, so the wait for a table was short. But when it comes to customer service, our waiter took forever to receive us. **Every meal took at least 15-20 minutes to come out.** I started to think they may did it on purpose to delay our hunger. 
My boyfriend and I ordered snails for appetizers, it was pretty okay not amazing or anything. Then, comes the salmon tartare (for someone who always tries this dish in every restaurant) I expected much more from a 15$ tartare. The taste was dull and the salmon was not fresh. 
For the main dishes, we ordered Filet Mignon and Seafood Pasta. Definitely not worth the money. The **filet mignon** was expensive, small and very difficult to eat. I had to chew non-stop my teeth were hurting. 

The desserts are what make the cut for me. A *succulent* chocolate cream and *mousse* with a very authentic orange and apricot cheesecake. 

**Final Verdict? Way overhyped in my opinion.**

_______________________

Les Enfants Terribles
1 Place Ville Marie, Montreal, QC H3B 4S6 
