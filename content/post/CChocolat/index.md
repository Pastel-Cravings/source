---
title: C'Chocolat
date: 2018-07-07T19:22:11-04:00
authors:
- Trang
categories:
- Restaurants
tags:
- Sweets
- Milkshake
- Chocolate
- Guy Concordia
description: Chocooo
banner: cchocolat1
---

Over the past two years, I've been to C'Chocolat at least 20 times and tried all of their special milkshakes. They announced this summer a new drink : **Ticket d'Or** (Golden Ticket) and I wanted to see if it was worth the buzz. 

Right away, the cotton candy is the first thing that attracted me. Very colorful and soft, I always loved that piece of sugar. Now to see it mixing with a lot of candies and more chocolate, it's heaven! 

The drink itself is pretty good, a dark chocolate flavour but with all the sweets around, the whole thing balanced itself. Not too bitter and not too sweet. 

The candies that they chose were all of my favourite candies, from strawberry flavours to cherries. 

**However, for a total of 20$, the drink is way too expensive.** That's the equivalent of two real meals combined (with proteins), but instead of that, you get a milkshake style and a lot of sugary garnitures. 
