---
authors:
- Trang
banner: cafespoon3
categories:
- Coffee Shops
date: 2017-10-14T10:10:50-04:00
description: I love the decor, immediately the combination of the green from the plants and the white modern look caught my eyes.
featured: 'true'
tags:
- Drinks
- Coffee Crawling
- Downtown Montreal
- Guy Concordia
- Peel
title: Cafe Spoon
---

Whenever I'm at Guy Concordia or Peel and I find myself searching for a coffee shop, this one is the first to pop in my head. Very accessible by metro, it only took me 5 mins to walk to Crescent St and find this little jewel. I love the decor, immediately the combination of the green from the plants and the white modern look caught my eyes. 

cafespoon1 Comptoir

They have a cute little space window where you can work and look outside and it emanates the feel of a greenhouse. 

cafespoon5 cafespoon3 "Window view"

A for the menu, they don't offer an exhaustive list of drinks but they have an amazing Soup Soup place just above the coffee shop (which is owned by the same franchise). In winter, I usually order a hot chocolate or chamomille tea and my boyfriend goes for an americano. 

cafespoon6 cafespoon7 "Boyfriend view" "Chamomille tea"
cafespoon2 Comptoir

## Overview

- [x] Good Wifi
- [x] Metro Accessibility (Via Guy Concordia) 
- [x] Nice Staff
- [x] Rating : 4/5 
