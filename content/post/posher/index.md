---
title: Posher
date: 2018-10-16T15:38:59-04:00
authors:
- Trang
categories:
- Restaurants
tags:
- Sushi
description: Sushi Experience in Cote Des Neiges
banner: posher7
---

My best friend Nhi introduced me to this place during one of our conversations and I had to check it out. 

## The Interior Design 

Right at the start, I was greatly impressed by the architecture and the design of the place. You would expect a small local sushi shop but what you get is a cool counter top bar rustic style. Wood panels on the wall along with cute plants in glasses floating the ceiling. Very well-thought design tables, it saves so much space yet fit so many seats. 

## The Poke Bowl 

The meal is definitely worth it, ratio quantity, quality - price. If you go at lunch, **for only 16$ you get a very BIG bowl of poke, miso and some shrimp tempura.** There are plenty of salmon, salads, and even crunchy potatoes chips on the side.

## The Beef Tartare 

**It was nicely presented but it was quite disappointing.** It is the TINIEST beef tartare I ever had. And I had quite a few around Montreal. It says it would come with 10 pieces of beef but if you look at the picture, each piece was the size of 5 cm. The beef tartare flavor is overshadowed by the amount of sauce placed in there. Overall, it is **way too expensive for its worth.**

## The Nigiri Plateau 

For the cost of 16$, you get a miso soup and a beautiful plate nigiri with syake,syake torché,izumidai, maguro, tamago, maguro torché, smoky philly salmon and ebi. The sauce was amazing. I'm a girl with a big appetite, so for me, it wasn't enough as a meal. But I do think that it's enough for other people. **If you have a big appetite, I would recommend taking the Poke Bowl and if you have a smaller stomach, this plate of nigiri.**

## Fans of Kpop

Oh yeah, you will be blasted with Kpop music with a huge screen playing the MVs of BTS and Blackpink ;) 





