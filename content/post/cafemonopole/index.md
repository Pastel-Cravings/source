---
authors:
- Kim S
banner: MonopoleCafeEtBuvette_FeaturedImage
categories:
- Coffee Shops
date: 2017-10-16T19:10:19-04:00
description: It has quickly become my go-to coffee shop for commuting to work between Old Montreal and Griffintown.
tags:
- Coffee Crawling
- Drinks
- Griffintown
title: Monopole café et buvette by Kim S
---

**Monopole is a coffee shop, restaurant and {bar in the Cité du Multimédia**, a neighbourhood consisting mostly of tech companies. It has been open since May 12th.

It has a nice selection of pastries and serves coffee from 49th Parallel and Bows and Arrows. It has quickly become my go-to coffee shop for commuting to work between Old Montreal and Griffintown. Lunch and dinner menus are also available with good quality food for reasonable prices. 

**Independent coffee shops are very rare in the area, so Monopole is a great addition to the neighbourhood.** For professionals in the area, it’s also a good stop after work to have a glass of wine.

---

Monopole Café
782 Wellington St, Montreal, QC H3C 2M8

MonopoleCafeEtBuvette_FeaturedImage "Monopole Cafe"
