---
authors:
- Trang
banner: aayokatoyokabai
categories:
- Restaurants
tags:
- Mont-Royal
- Ramen
- Le Plateau
date: 2018-06-27T00:00:46-05:00
description: Best Ramen in Montreal
title: Yokato Yokabai
address: ["Yokato Yokabai", "4185 Drolet", "Montreal", "QC H2W 2L5"]
---

**This is not your typical food review. This is a love story. Be warned.**

Yokato Yokabai opened druing a time where I just recently discovered Japanese Ramen. I tried a few in town already (Misoya Ramen and Takanara Ramen) so I was curious about this place. 

When I entered, **I was greeted by a  woody ramen bar setting.** You can see the cooks doing their work and a waiter would seat you at the counter or at the few tables available. 

They don't have the usual menu. Instead, they have direct order papers where you would  write your name at the top and circle what you want on the paper. I circled **the tonkotsu broth with terriyaki chicken as the meat, more salted broth option and one marinated egg**. 

The bowl came and I followed the waiter's advice of putting some extra toppings and then, I dive right in. I will always remember the first time where that broth hit my taste pallet. **FOOOODGASSMMM**. It's everything I've ever loved. **Creamy but not too heavy on the stomach, a light flavor of pork but so aromatic**. Surprisingly, **the chicken is super tender** and juicy (they keep the fat which is A BIG PLUS for me because I love that part).

Now,let's talk about those DAMN TASTY NOODLES. I don't know how to describe it with words. But I'll try my best. It's thin noodles are smooth,gentle and delicate. I don't f* know how they create it but the noodles are lit. 

However, the real *coup de coeur* of the visit, is the marinated egg. YUP. **This restaurant started my obession with eggs.** Their marinated sauce looks so simple yet it adds such a great texture to the exprience. Soft boiled, not overcooked and super jammy-yolked.
