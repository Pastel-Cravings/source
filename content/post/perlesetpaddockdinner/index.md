---
title: Perles et Paddock 
date: 2019-02-26T14:42:22-05:00
authors:
- Trang
categories:
- Restaurants
- Bars
- Coffee Shops
- Others
tags:
- Dinner
- Fine Dining
description: New Tasting Menu 
banner: featuresperlesetpaddock
featured: 'true'
---

## Great service, great food, what more do we need?  

Last year I had the chance to write a post about the brunch at Perles et Paddock, I loved my experience and I had to come back for dinner. The timing was just great because they just added a new Tasting Menu and I had the chance to try it!

They offer 3 options: vegetarian, mix and an option with wine pairing as well. We chose the mix menu with the wine pairing. 

The first Amuse Bouche we had is **Poultry Mousse, with a delicious runny marinated quail egg**. Paired with a classic cocktail called The Bamboo, with orange bitters with black garlic. The other pairing was a savoury Reverse Manhattan, with a sweet vermouth (produced locally). 

Following by the **Goat Espuma, whisked to lemon and honey fir**. It has a little sweeet aftertaste which makes everything light and ends on a good note. 

Then comes the Confit Leeks, with Quebec mozzarella, Brussels’ sprouts and tarragon/ My highlight and probably one of my favourites meals of all times: **Roasted Arctic Char with 
dashi broth, daikon and enoki.** What a discovery! I’m usually very picky on my cooked fish but **I can officially say that this is my favourite main dish in all the restaurants in Montreal so far.** 

I have noticed several times in the tasting menus from other fine dining places, most often, the plates I love the most are always appetizers or desserts. Never the main course and that’s such a shame since I want to be impressed by the main part. Perles et Paddock surprised me with its main courses highly rich in flavours, memorable enough to sit at the top of my restaurants list right now. 

To finish the night on a lighter note, we had **3 sweet canapés**: pavlova lychee sorbet, oatmeal tartlet with beer jelly on the inside and Chantilly, ending with a dark chocolate financier noir with hazelnut and caramel. 

Overall, I find this to be one the most worth it degustation and wine pairing menu. 


A big thank you to Perles et Paddock for having us and to the staff for such a warm welcome ! 

