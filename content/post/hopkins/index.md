---
title: "Hopkins"
date: 2019-02-21T16:17:04-05:00
authors:
- Trang
categories:
- Restaurants
- Bars
tags:
- Dinner
description: Chic and Modern in NDG 
banner: restauranthopkinsheader
featured: ‘false’

---

## Interior Design 

Efficiency is the keyword in Hopkins design. Enter a cozy space but they managed to include only necessary details to express their creative minds: beautiful ceiling mirror to the Scandinavian counter chairs, small lights reflecting from the back of the bar. Everything is made to give a modern yet chic atmosphere. 

## Dinner Menu 

We started out the night with **a charcuterie platter, the beef tartare, eggplant à la bello  and beef bone marrow.** Everything was flavourful and delicious. I’m not usually a fan of eggplants, but somehow these ones manage to make the cut: it’s crispy, a little bit sweet. But let me tell you about my favourite appetizer of all times: **Foie Gras Deviled Eggs With Smoked Caviar**. It’s a meal you didn’t know you needed in your life until now. 

 The main dishes were Truffles Gnocchi and Lobster Fettuccine, although I find the gnocchi a bit too salty I really enjoyed the fettuccine. My favourite will still be the Filet Mignon and Foie Gras, very tender and the Foie Gras just adds all the flavours. 

We also a beautiful dessert platter in the end, accompanied by some coffee shots: Hot Shots. Yes real shots. One of the most original desserts I’ve had. 

Thank you to Hopkins Restaurant and the staff for this wonderful exprience
