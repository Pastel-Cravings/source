---
title: Les Gourmandises De Peyton
date: 2018-07-01T14:51:15-04:00
authors:
- Trang
categories:
- Others
- Coffee Shops
tags:
- Sherbrooke
- Smoothies
description: Sugary and Instagram Smoothie
banner: gourmandisespeyton3
---

6 minutes walk from metro Sherbrooke, *Les Gourmandises de Peyton* is a small delicate pastry and coffee shop. I wanted to go there to try their "Magic Smoothies" that Instagram has been sharing non-stop these days. 

When you have an intense heat like 35 degrees celsius and you decide to leave **the door open with no AC, it was kind of hard to not suffocate inside the shop.** I was sitting down for 5 minutes and I had to go outside and take the pictures. I couldn't tolerate the heat, but of course, that may be just me. 

The smoothie itself was quite ordinary: strawberries and some chia seed. **Honestly for a drink that costs me more than 10$, I expected an explosion of flavors.** What I got instead was a lot of sugar on top (the little donut was good enough), some popcorns and candies. **Other than that the drink itself didn't taste like anything.** It was so diluted that I couldn't even taste the strawberries. 

**Is it Instagrammy? Yes. Is worth it ? No**. I know that you are going to go there anyway so **my advice is this : spend money on one smoothie and share it among your friends ;).**
