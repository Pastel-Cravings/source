---
title: Miss Wong
date: 2018-10-16T15:39:54-04:00
authors: [Trang]
categories:
- Restaurants
tags:
- Drinks
description: Incredible Interior Design but Average Food
banner: misswongbanner

---

Unfortunately Miss Wong was a miss for me.  We went there for my birthday and at first, the decor will blow you away : different concepts on each corner of the restaurant, a lot of fun to take pictures of but as for the food, it was simply ordinary. As an Asian girl, I could seriously got a better food at home. The first starters were dumplings and they were very disappointing with a powdery texture. 


The fried calamari was good enough to be the highlight of my day but I thin you can hardly miss a typical meal as fried calamari. On top of that, the service was super slow for our table. I wanted a simple coke and I had to remind them 3 times to get the drink for me. 

Overall, the price is expensive, the food is average and the customer service we received was not the best. You can quickly skip this venue and explore some of other Montreal restaurants worth your time. 