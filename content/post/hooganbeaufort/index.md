---
title: Hoogan et Beaufort
date: 2018-10-16T15:39:30-04:00
authors:
- Trang
categories:
- Restaurants
tags:
- Brunch
description: Delicate Brunch in Angus
banner: IMG_0371
---

## Interior Design

Right away, you will notice the dark walls and the modern design of the place. It surprisingly fits a lot of people for a small space. The staff was super nice and we were welcome with a big smile. I will advise to call and reserve your seats in advance. 


## Le Brunch 

*Note that the brunch is only available on the weekend.* 

As appetizer, we started with a focaccia and a decadent homemade caramel sauce (5$). That caramel ALONE was worth the trip. 

Moving on to the main dish, I was looking for the *perfect scrambled eggs* when I visit this brunch place and oh my I was not disappointed. Runny, soft and feels like a cloud of milk and fluffiness. The flank steak was juicy, decadent and perfectly compliment the eggs and the brussel sprout. Oh, and I just loved the sauce. I mean you wouldn't notice it but that sauce IS MAGICAL. It makes the veggies, the steak and the eggs has an after taste of grilled and marinated at the same time. 

You can always count on my boyfriend to order the classic brunch for 15$ : sunny side up eggs and bacon. Like they said, if a restaurant makes the most classical meal a *delice*, everything else should fall in line. And it did. 

Address: 
4095 Rue Molson, Montréal, QC H1Y 3L1
The simplest way to get there is to directly go to William - Tremblay street and walk in the little outlet. 
Commute: 12 mins walk from Metro Prefontaine.


