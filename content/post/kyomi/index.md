---
authors:
- Trang
banner: fpadthaikyomi
categories:
- Restaurants
tags:
- Dinner
- Sushi
- Lasalle
- BYOW
date: 2018-06-26T14:06:46-05:00
description: BYOW Japanese and Vietnamese Delight
title: Kyomi
---

**UPDATED** 

I also went at noon to try their lunch menu with my parents :) ! 
**The wonton soup** as a starter works really well on setting the mood. Followed by some delicious **gyozas** (fried japanese dumplings). These were an absolute favourites of our families because the sauce that comes with it was a bit sweet and sour at the same time. 

The imperial rolls have all the flavours that come with the typical vietnamese experience. 

As for the main meals, I sticked with salmon tartare (I just fell in love with it since last time I was here (see the review from January below). My mom had ordered the grilled beef and it comes in a stone platter and smoke was still coming out of it. The beef was very tender and true to the vietnamese signature. As for my dad, he went for a shrimp pad thai served with a fried rice galette. 

**The Dinner Menu - Previous Review From January 2018** 


When you step in the restaurant, it's like entering a whole new world. The decor screams romantic and modern asian fusion. It's the perfect place to bring in your date or a family gathering. 

I ordered the **Salmon Tartare** (meal version comes with sweet potatoes so why not?! :D) AND THIS RIGHT HERE IS HEAVEN. It's sweet and light, the asian inspiration crispy crackers go very well with this tartare.  

The sushis were so good and in big portions too! By big I mean it's fulfilling and not tiny pieces like other sushi restaurants. We were on a double date with our two favourite vegans so yes they do offer a vegan menu! 

For dessert, we went for Creme Brulee and Fried Banana with Ice Cream. 
I was not a fan of how they overcooked the surface of our Creme Brulee. I tend to be a bit picky about this dessert since it's one of my favourite hahah. 
**About**
Kyomi Restaurant
"7660 Newman Blvd, Lasalle, QC H8N 1X8"
http://kyomirestaurant.com/

## Overall

Great food, super tartare and my advice is to rbign a few friends with you to try this extensive menu! ;) 

Special thank you to My Hanh, the restaurant's manager for this lovely evening! 
