---
title: LOV
date: 2018-11-22T15:39:13-04:00
authors:
- Trang
categories:
- Restaurants
tags:
- Brunch
description: Vegan Instagrammy Restaurant
banner: IMG_1852
---


## Interior Design

They really put a lot of efforts to convey an unique green leaves and plants with open spaces in their restaurants. The decorations and designs just fit perfectly. With their signature cute swing chairs and the aqua pastel and yellow green theme. That said, it's no wonder that every foodstagrammer I know has step foot in LOV at least ONCE in their lives. 

## My Personal Opinion 

**Did I like the food?** Yes. **I love the pesto gnocchi and the mac'n cheese from the brunch menu.** **But, it is too expensive** for what they have to offer. I've been to other vegan restaurants in Montreal and there was a balanced price-quality ratio. The food is good but not phenomenal, quite a dull experience.     

## Instagram Worthy Over Quality? 

I always wanted to try the restaurant so it's no surprise that I invited my favourite couple: Anton and Dominique, who  also happen to be vegan . I love them with all my heart because they are the kind of vegan friends who are super open-minded and will never rub in your face or make you feel ashamed about what you eat. They are also the ones who introduced to all these latest news about vegan food and even motivated me to visit my first Vegan Fest. 

**They left LOV being very unsatisfied with the food.** It's certainly nothing new to them, or even worse, the food they do at home is quite better. 

I think everyone is going to give this restaurant a try and see it for themselves despite all the negative sides. But I cannot lie, the design makes pretty pictures :) 

They have 3 different restaurants, 2 in Montreal and one in Laval. I prefer leaving their websites below and you can see for yourself which one is closer for you. 

Website : https://www.lov.com/
Brunch Menu available on the weekend. 


