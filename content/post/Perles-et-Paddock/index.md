---
title: Perles Et Paddock
date: 2018-07-02T10:48:22-04:00
authors:
- Trang
categories:
- Restaurants
- Bars
tags:
- Brunch
- French Cuisine
description: Perfect Open-Air Brunch
banner: pelespaddock1
---

Picture a sunny day and all you want is a brunch and a beautiful open air space. But you get tired of the terrace because it's hot as hell outside and because of all the air pollution. That's when I found an alternative: Perles et Paddock. 

It's a squared building with an open space entry. The view caught my eyes from outside already. However, the magic really begins when you enter the site. It's beautiful as it can get. **You are welcomed with green leaves all around and an open-bar with a whole tree standing besides it.** Okay let me specify, open bar in the Litteral sense of term, as in the ceiling-open-air. The ceiling is made of glass to let the sunshine comes in naturally. That way you get to enjoy not only your **mimosa** in the sun but also eat delicious food.

Whenever I visit a restaurant, my first goal is to eat most of the things on the menu! Are you ready ? Because we ordered A-LOT. 

We started with a **[dozen  oysters](/post/perles-et-paddock/#post-img-0)**, which I learned, are coming from New-Brunswick. They were so fresh and the vinegar compliments the saltiness of the sea. 

Then, **Tony** *(shout out to my foodie homie who didn't want to appear in my pictures so I respect that haha)* convinced me to try **boudin**(blood sausage)! I never wanted to try before but I'm glad I did. The **texture is soft reminding me of Vienna sausage and the meat is richly seasoned.** The caramelized pear and kale wrap up the earthly feeling nicely. 

Coming from Bretagne, I had to order their **Pain Perdu**. A nice bundle of bread served in a white egg sauced and cinnamon. My excitement starts to show when I finally cut that bread to reveal the buttery side of it. To taste it, was even a more amazing experience. The butter makes it so soft and fluffy to eat and the impressive part is, it's not overly sweet. It has a very light taste. 

Then, comes the **pancakes**. Again, super light and super soft pancake. I don't think I've ever tasted a more perfect texture. It's crispy on the edge but soft on the inside. The strawberries are small and so sweet! 

And.. we are the weirdos who finish their meals with **Chicken Waffles and Foie Gras** ! The foie gras is as majestic as the picture shows. One of the best Foie Gras I've had in my life and very close to the ones you can find in France. The sponge you see in the picture is actually sugar! Also named as sponge candy, it tastes like a delicate toffee. 




