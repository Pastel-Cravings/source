---
title: Muru Crepe
authors:
- Trang
categories:
- Restaurants
- Coffee Shops
tags:
- Sweet
- Champ-de-mars
- Old-Port
description: Cute Arts Matcha Coffee and Sweets 
banner: cmurucrepe
featured: 'true'
date: 2018-06-25T15:36:23-04:00
address: ["Muru Crepe", "362 Notre-Dame St E", "Montreal, QC H2Y 1C7"]
---
**TOTORO!** That was what I screamed out when I first received my matcha latte. The details are so impressive! For coffee lovers, you will surely find a drink fits to your needs

However, the highlight of my visit is their Weekend Special **Creme Brulee Crepe**. I discovered my first Crepes Bretonnes when I live in Rennes, Bretagne for two years. I'm always looking for delicious crepes and when I see that they decide to combine Creme brulee AND crepes?! I had to try it.

The crepe as it is, is very soft and pretty close to what I ate in France. It holds the Creme Brulee like a cup. I was a bit confused at first on how to approach this meal. *Should I eat  the creme brulee first with a spoon?* Should I just hold the crepe and eat the whole thing? I decided to play safe and eat it with a spoon. The creme brulee flavour is only obvious at the top, the rest at the bottom is a mix of whipped cream, banana and some English cream. 

Overall, it was a decent meal with a total cost of **20$**. I would say it is quite expensive for one dessert and one coffee
