---
title: Barley
date: 2018-09-26T15:08:01-04:00
authors:
- Trang
categories:
- Restaurants
tags:
- Brunch
description: Cereal Bar and Matcha Pancakes At Your Service
banner: barley4
---

I discovered this lovely place in the Little Burgundy neighborhood and I loved every single bit of it. 

## The Interior Design 

The layout of the restaurant is what stands out the most for me. As you push open the door, an aisle entrance welcomes you. Small circles tables on the right with a beautiful view and a giant window decorated with some greens and leaves. On the right, a big circle mirror on the wall (selfies time, girls!) Down the alley, there are some comfy couch-like booths and the beginning of a larger rectangle area. 

On the right, with glass doors and windows, you can see a marketing agency office and at the center of the area is the whole cereal bar and more tables to seat on. It feels spacious and warm. 

## Brunch Special - Matcha Pancakes 

You can find the regular menu on the Barley website, but the special brunch menu is only offered on the weekend. One particular item that has caught my eyes and stole my heart is the **Matcha Pancakes**. I am a Matcha lover, and this was the highlight of my weekend. Not only the pancakes are soft, but the matcha flavor is light and flavorful. 

**The thing with Matcha Powder is, everyone thinks that it's easy to cook with it but nobody really understands it.**

In order to bake and cook with Matcha Powder, you have to understand how it tastes and how to balance it with other ingredients. Too much, and it becomes like you just inhale a ton of powder and sugar, too little and you won't even taste a difference. Barley has mastered the perfect balance of it. You can taste the flavor but it's not too sweet. 

## Smoothie Bowls and Cereals 

During the week, you can still have a large variety of items from exotic smoothie bowls to delicious cereal mix. My ultimate *coup de coeur* is **Le Safari** composed of mango, pineapple, banana, greek yogurt,
coconut milk and honey almond granola. I honestly don't like smoothies usually but I finished this bowl in one bite. It was THAT good! 





