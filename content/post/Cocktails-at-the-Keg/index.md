---
title: Cocktails At The Keg
authors:
- Trang
categories:
- Restaurants
- Bars
tags:
- Bonaventure
- McGill
description: Cocktail Menu Launch Event
banner: cthekegfoodcoctails
featured: 'true'
date: 2018-06-22T15:36:23-04:00
---

I had the chance to be invited by my friend My for this cute get-together to celebrate the new cocktails menu of The Keg- Place Ville Marie and the opening of their new terrace ! Yay Summer ! 

The drinks were served in a beautiful setup. My favourite has to be blueberry cocktail as shown in the picture. It's sweet and sparkling, it screams summer! 

That's not it, there were also foooood! YAS. You can taste the mini burgers and smoked meat, along with delicious fresh shrimp in a tomatoe sauce, a fried rice ball and many more appetizers. 

 The most amazing part is you can spot all the infuencers and bloggers taking out their phones and cameras haha! I felt at home to be honest. No one to judge us how long we are taking them pictures #bloggersquad ;) 


The Keg - Place Ville-Marie
5 Place Ville Marie, Montreal, 
QC H3B 2G2

Close to Metro Bonavenure - Metro McGill 

