---
authors:
- Anna
banner: RegineCafe_Feauturedimage
categories:
- Coffee Shops
date: 2017-11-20T20:02:03-05:00
description: Guest Post by Anna
tags:
- Brunch
- Coffee Crawling
- D'Iberville
- Beaubien
title: Régine Café by Anna
---

This place is by far, my favorite brunch spot for numerous reasons. First of all, the menu offers many interesting and creative choices, such as Scottish eggs, brioche bread, pulled ham, trout gravlax and mascarpone cheese cream. Even their oatmeal, made with coconut milk and mango jam, tastes spectacular. Every bite is heaven and the presentation is gorgeous. For those who are gluten intolerance, they offer a few gluten-free options.


Besides the original dishes, the staff is friendly and the ambiance is very retro-chic. This is the place to impress a foodie on a date. The only downside: the crazy line up. Indeed,if you decide to go during the weekend, be prepared to wait for at least half an hour.Often, the staff would distribute warm tea and pastry samples for those waiting in line outside during cold weather.

---

Regine Cafe
1840 Rue Beaubien E, Montréal, QC H2G 1L6"
http://reginecafe.ca/
