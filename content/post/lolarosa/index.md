---
title: Lola Rosa 
authors:
- Trang
categories:
- Restaurants
tags:
- Mont Royal
- Le Plateau
- Vegan
description: Casual Vegan Eat
banner: clolarosanachos
featured: 'true'
date: 2018-06-23T15:36:23-04:00
---
Once every 6 months, I love meeting Dominique and Anton my two good friends who happen to also be vegan! We always take the opportunity to discover the Vegan side of our city. For today, it is Lola Rosa, highly praised by Dominique and I completely understand why. 

I ordered a Lychee cocktail to start with and Anton has ordered this very strange drink where they put a whole PEPPER in iti YES. PEPPER. He said it's one of his faves! 

For the main course, I got the Nachos (the small one okay) at 12$. But, oh gosh. It was so big! I swear guys, you can easily share this meal for two. At only 12$!! And I didn't order their Mountain version yet hah! 

Their burrito ordered by William is also excellent and my favourite discovery is probably those fries that Dominique made me try. 

For dessert we had Cheesecake in a jar and Pistachio cream ! Very light and sweet at the same time. I absolutely love the little salty touch bringing by the crunchy graham on top. 

