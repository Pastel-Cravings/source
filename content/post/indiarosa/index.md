---
title: India Rosa
date: 2019-02-06T16:14:27-05:00
authors:
- Trang
categories:
- Restaurants
- Bars

tags:
- Le Plateau
- Mont Royal
- Dinner
- Brunch
- Indian Cuisine
- Drinks
description: Succulent Indian Food and Imaginative Cocktails
banner: indiarosa
featured: 'true'
---

## Interior Design

As soon as you enter the restaurant, you will be welcomed in cozy and enchanting atmosphere. Bird cage style hanging chairs, green leaves and cute little candle lights, the perfect spot for a date or even a get-together with friends. I had the chance to go there at brunch and at dinner, it was a great experience both times. 


## The Food 

As a big Indian food fan, I was very pleased with the overall menu. You have the appetizers to share, we had the shrimps Malai and tandoori mushrooms. Both are delicious, **especially the shrimps Malai **. I’m officially obsessed with the creamy and buttery sauce.  

We tried the saag paneer , which is Homemade Indian cheese in a spinach and onion sauce. Unfortunately, I couldn’t taste the cheese much, it was overshadowed by all of the spinach and the sauce. 

## Meals to share 

Of course we had to try the butter chicken, it is tender and buttery which I liked in butter chickens. All of their meals go well with the naan bread, by the way, fluffy naan bread! They come in a several variations : garlic, cheese, paratha, chapati and onions. I love mine simple so my favourite in the regular ones.   

The Korma Lamb has a very interesting flavor, it’s soft and creamy, very light and easy to eat. 

## Vegan Options Available

Even my vegan friends approved of the menu items, we had samossa (definitely going back for them!), vegan biryani (Basmati rice cooked with mix vegetables) and chana masala (chickpeas with spices and herbs). 