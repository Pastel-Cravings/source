---
title: Others
---
"We begin to learn wisely when we're willing
to see world from other people's perspective."
- Toba Beta
